VM=m2stat_gd.ova

all: vm

vm: vm-clean vm-up vm-ova

vm-ova: $(VM)

vm-clean:
	vagrant destroy -f || true
	rm $(VM) || true

vm-up:
	DISPLAY=:0 vagrant up

%.ova:
	vagrant halt
	vboxmanage sharedfolder remove $(shell cat .vagrant/machines/default/virtualbox/id) --name vagrant || true
	vboxmanage export $(shell cat .vagrant/machines/default/virtualbox/id) -o $@

