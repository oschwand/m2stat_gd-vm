---
- name: Install desktop environment
  hosts: all
  tasks:
  - name: Setup timezone
    ansible.builtin.command: setup-timezone -z Europe/Paris
    changed_when: false
  - name: Setup Xorg base
    ansible.builtin.command: setup-xorg-base
    changed_when: false
  - name: Install video driver
    ansible.builtin.apk:
      name: virtualbox-guest-additions
      state: latest
  - name: Install desktop packages
    ansible.builtin.apk:
      name: lxdm,dbus,dbus-x11,setxkbmap,xrandr,firefox-esr,vim,xterm,ttf-dejavu,openbox,tint2
      state: latest
  - name: Enable dbus service
    ansible.builtin.service:
      name: dbus
      enabled: yes
      state: started
  - name: Configure lxdm for autologin
    ansible.builtin.lineinfile:
      path: /etc/lxdm/lxdm.conf
      regexp: '^# autologin='
      line: autologin=vagrant
  - name: Configure lxdm to start openbox
    ansible.builtin.lineinfile:
      path: /etc/lxdm/lxdm.conf
      regexp: '^# session='
      line: session=/usr/bin/openbox-session
  - name: Configure keyboard layout
    copy:
      dest: /etc/X11/xorg.conf
      content: |
        Section "InputClass"
          Identifier "Keyboard Default"
          MatchIsKeyboard "yes"
          Option "XkbLayout" "fr"
          Option "XkbVariant" "oss"
        EndSection
  - name: Add user to input and video groups
    ansible.builtin.user:
      name: vagrant
      groups: input,video
      append: yes
  - name: Configuration directory for openbox
    ansible.builtin.file:
      path: /home/vagrant/.config/openbox
      state: directory
      owner: vagrant
  - name: Autostart in openbox
    ansible.builtin.copy:
      dest: /home/vagrant/.config/openbox/autostart
      mode: "0755"
      content: |
        xrandr --output VGA-1 --mode 1600x900
        tint2 &
  - name: Remove tint2 configuration tool
    ansible.builtin.file:
      path: /usr/bin/tint2conf
      state: absent
  - name: Minimal menu for openbox
    ansible.builtin.copy:
      dest: /home/vagrant/.config/openbox/menu.xml
      content: |
        <?xml version="1.0" encoding="UTF-8"?>
        
        <openbox_menu xmlns="http://openbox.org/3.4/menu">
        
        <menu id="root-menu" label="Openbox 3">
          <item label="Terminal">
            <action name="Execute">
              <command>xterm</command>
              <startupnotify><enabled>yes</enabled></startupnotify>
            </action>
          </item>
          <item label="Jupyter">
            <action name="Execute">
              <command>jupyter notebook</command>
              <startupnotify><enabled>yes</enabled></startupnotify>
            </action>
          </item>
          <item label="Firefox">
            <action name="Execute">
              <command>firefox</command>
              <startupnotify><enabled>yes</enabled></startupnotify>
            </action>
          </item>
          <item label="Resize display">
            <action name="Execute">
              <command>xrandr --output VGA-1 --auto</command>
              <startupnotify><enabled>yes</enabled></startupnotify>
            </action>
          </item>
          <item label="Upgrade virtual machine">
            <action name="Execute">
              <command>xterm -e "sudo /home/vagrant/upgrade.sh; echo Press any key to close the window; read"</command>
              <startupnotify><enabled>yes</enabled></startupnotify>
            </action>
          </item>
          <item label="Quit">
            <action name="Execute">
              <command>sudo halt</command>
              <startupnotify><enabled>yes</enabled></startupnotify>
              <prompt>Halt virtual machine ?</prompt>
            </action>
          </item>
        </menu>
        
        </openbox_menu>
  - name: Enable lxdm service
    ansible.builtin.service:
      name: lxdm
      enabled: yes
      state: started

- name: Install misc tools
  hosts: all
  tasks:
  - name: Misc packages
    ansible.builtin.apk:
      name: unzip,vim,py3-pandas,py3-pyzmq,py3-psycopg2,py3-argon2-cffi
  - name: Install Jupyter
    ansible.builtin.pip:
      name: notebook

- name: TP SQL
  hosts: all
  tasks:
  - name: Install sqlite and pgsql packages
    ansible.builtin.apk:
      name: sqlite,postgresql14
      state: latest
  - name: Enable postgresql service
    ansible.builtin.service:
      name: postgresql
      enabled: yes
      state: started
  - name: Create a new database with name "vagrant"
    community.general.postgresql_db:
      name: vagrant
  - name: Grand all pgsql privileges to user vagrant
    community.general.postgresql_user:
      db: vagrant
      name: vagrant
      priv: ALL
      state: present
  - name: Install python driver for postgresql
    ansible.builtin.apk:
      name: py3-psycopg2

- name: TP noSQL
  hosts: all
  tasks:
  - name: Enable outdated Alpine 3.9 repository
    ansible.builtin.blockinfile:
      path: /etc/apk/repositories
      state: present
      block: |
        https://sjc.edge.kernel.org/alpine/v3.9/main
        https://sjc.edge.kernel.org/alpine/v3.9/community
  - name: Install mongodb from outdated Alpine repository
    community.general.apk:
      name: mongodb-tools,mongodb,yaml-cpp=0.6.2-r2
      state: latest
  - name: Enable mongodb service
    ansible.builtin.service:
      name: mongodb
      enabled: yes
      state: started
  - name: Install Python driver for mongodb
    ansible.builtin.pip:
      name: pymongo

- name: TP webscrapping
  hosts: all
  tasks:
  - name: Install requests and bs4 Python packages
    ansible.builtin.pip:
      name: requests,bs4

- name: TP Spark
  hosts: all
  tasks:
  - name: Install java
    ansible.builtin.apk:
      name: openjdk11
  - name: Set JAVA_HOME
    ansible.builtin.lineinfile:
      path: /etc/environment
      state: present
      line: JAVA_HOME=/usr/lib/jvm/default-jvm
  - name: Install pyspark Python packages
    ansible.builtin.pip:
      name: pyspark[pandas_on_spark],findspark
    become: yes
    become_user: vagrant

- name: Common tests
  hosts: all
  tasks:
  - name: Basic notebook for tests
    ansible.builtin.copy:
      src: Tests.ipynb
      dest: /home/vagrant/Tests.ipynb
      mode: 0644

- name: Upgrade logic
  hosts: all
  tasks:
  - name: Install ansible and git
    ansible.builtin.apk:
      name: ansible, git
  - name: Directory for ansible files
    ansible.builtin.file:
      path: /opt/ansible
      state: directory
  - name: Git checkout
    ansible.builtin.git:
      repo: https://codeberg.org/oschwand/m2stat_gd-vm.git
      dest: /opt/ansible
  - name: Upgrade script
    ansible.builtin.copy:
      dest: /home/vagrant/upgrade.sh
      mode: "0755"
      content: |
        cd /opt/ansible
        git pull
        ansible-playbook upgrade.yml

- name: Cleaning
  hosts: all
  tasks:
  - name: Remove user cache directory
    ansible.builtin.file:
      path: /home/vagrant/.cache
      state: absent
  - name: Remove root cache directory
    ansible.builtin.file:
      path: /root/.cache
      state: absent
  - name: Remove system cache directory
    ansible.builtin.file:
      path: /var/cache
      state: absent
  - name: Zeroing free space
    ansible.builtin.shell: dd if=/dev/zero of=/empty bs=1k count=20G; rm /empty
    changed_when: false
